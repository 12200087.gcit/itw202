import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


function Home({navigation}){
  return(
    <View style={styles.screen}>
      <Text>Welcome HomeScreen</Text>
      <Button 
      title='Go to Profile'
      onPress={()=>{
        navigation.navigate("Profile")
        }
      }/>
      {/* <Button 
      title='Go to Contact'
      onPress={()=>{
        navigation.reset({
          index:0,
          routes: [{name: 'Contact'}]
        });
        }
      }/> */}
    </View>
  );
}
function Profile({navigation}){
  return(
    <View style={styles.screen}>
      <Text>Welcome ProfileScreen</Text>
      {/* <Button 
      title='Go Back'
      onPress={()=>{
        navigation.goBack();
        }
      }/> */}
      <Button 
      title='Go to Contact'
      onPress={()=>{
        navigation.replace("Contact")
        }
      }/>
    </View>
  );
}
function Contact({navigation}){
  return(
    <View style={styles.screen}>
      <Text>Welcome ContactScreen</Text>
      {/* <Button 
      title='Go to Home'
      onPress={()=>{
        navigation.navigate("Home")}}/> */}
      <Button 
      title='Go Back'
      onPress={()=>{
        navigation.goBack();
        }
      }/>
    </View>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Home' screenOptions={{headerShown:false}}>
        <Stack.Screen name='Home' component={Home}/>
        <Stack.Screen name='Profile' component={Profile}/>
        <Stack.Screen name='Contact' component={Contact}/>
      </Stack.Navigator>
    </NavigationContainer>
    
  );
}
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  screen: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
