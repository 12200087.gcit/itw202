import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import CountHook from './components/CountHooks';
export default function App() {
  return (
    <View style={styles.container}>
      <Text style={{fontWeight:'bold'}}>Hooks</Text>
      <CountHook/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});