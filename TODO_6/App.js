import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View, Text } from 'react-native';

export default function App() {
  return (
   <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.flex0}><Text style={styles.text}>1</Text></View>
      <View style={styles.flex1}><Text style={styles.text}>2</Text></View>
      <View style={styles.flex2}><Text style={styles.text}>3</Text></View>
   </View>  
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection:'row',
    marginTop:100,
    marginLeft:50
  },

  text: {
    textAlign:'center',  
    marginTop:100
  },
  flex0: {
    backgroundColor: 'red',
    height:200,
    width:70,

  },
  flex1: {
    backgroundColor: 'blue',
    height:200,
    width:120,

  },
  flex2: {
    backgroundColor: 'green',
    height:200,
    width:20,

  },
});
