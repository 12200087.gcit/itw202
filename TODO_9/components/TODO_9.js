import { View, Text, StyleSheet,Image } from 'react-native'
import React from 'react'

const TODO_9 = () => {
  return (
    <View>
      <Image style={styles.image1} source={{uri:'https://picsum.photos/100/100'}}/>
      <Image style={styles.image2} source={require('../assets/react_native.png')}/>
    </View>
  )
}

export default TODO_9

const styles=StyleSheet.create({
    image1:{
        width:100,
        height:100
    },
    image2:{
        width:100,
        height:100,
        marginTop:10
       
    }
    
})