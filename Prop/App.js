import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Courses from './components/courses';
import CountClass from './components/countClass';

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={{fontWeight:'bold'}}>Props</Text>
      <Courses></Courses>
      <Text style={{fontWeight:'bold'}}>States</Text>
      <CountClass/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
