import { useState } from 'react'
import { View, Text,TextInput, StyleSheet } from 'react-native'
import React from 'react'

export default function TODO_10() {
    const [text, setText]=useState(' ');
    const [name, setName]=useState(false);
  return (
    <View>
      {name && <Text style={styles.text}>Hi! {text}</Text>}
      <Text>What is your name?</Text>
      <TextInput secureTextEntry style={styles.textinput} placeholder=' ' onSubmitEditing={()=>setName(!name)}
      onChangeText={(value)=>setText(value)}/>
    </View>
  ) 
}
const styles = StyleSheet.create({
    textinput:{
        borderColor:'black',
        borderWidth:1,
        height:35
    },
    text:{
      margin:50
    }
})