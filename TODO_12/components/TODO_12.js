import { View, Text, StyleSheet } from 'react-native'
import React,{useState} from 'react'
import { SafeAreaView,TextInput,TouchableOpacity,ScrollView ,StatusBar} from 'react-native'

export default function TODO_12() {

    const[inputText,setInput]=useState(' ');
    const[text,setText]=useState([]);
    const handleText=(inputText)=>{
        setInput(inputText);
    }
    const handleSubmit=()=>{
        setText(current=>[...text,inputText])
    }
    const cancel=()=>{
        setInput(' ')
    }

  return (
      <ScrollView>
            <View style={styles.container}>
              <TextInput 
              style={styles.input}
              value={inputText}
              onChangeText={handleText} />

              <View style={styles.buttonview}>
              
              <TouchableOpacity onPress={cancel}>
                    <Text style={{color:'red',fontSize:20,marginLeft:70}}>Cancel</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={handleSubmit}>
                    <Text style={{color:'blue',fontSize:20, marginLeft:20}}>Add</Text>
              </TouchableOpacity>
             
              </View>

              <View style={{marginTop:20}}>
              
                  {text.map((inputText)=>
                  
                   <View style={styles.displayview}>
                     
                      <Text style={{color:'white'}} key={inputText}>{inputText}</Text>

                    </View>)}                  
              </View> 
            </View>            
      </ScrollView> 
  )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:300,
      paddingTop: StatusBar.currentHeight,
    },
    input:{
        borderWidth:1,
        borderColor:'black',
        height:35, 
        width:300
    },
    buttonview:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row',
        marginRight:100,
        
    },
    displayview:{
        padding:10,
        borderColor:'black',
        borderWidth:1,
        backgroundColor:'grey',
    }
  });
