import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { add, multiply } from './component/nameAnddefault';
import division from './component/nameAnddefault';
import Courses from './component/funcComponents';
export default function App() {
  return (
    <View style={styles.container}>

      <Text style={{fontWeight:'bold'}}>My first Project!</Text>
      <Text>Result of addition : {add(3,4)}</Text>
      <Text>Result of Multiplication : {multiply(7,3)}</Text>
      <Text>Result of division : {division(14,2)}</Text>
      <StatusBar style="auto"/>
      

      <Text style={{fontWeight:'bold'}}>Function Component</Text>

      <Courses></Courses>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
