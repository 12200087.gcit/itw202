import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.view1}/>
      <View style={styles.view2}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  view1:{
   
    backgroundColor:'red',
    height:100,
    width:100
  },
  view2:{
  
    backgroundColor:'blue',
    height:100,
    width:100
  }
});
