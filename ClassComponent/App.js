import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {
  render= () => (
    <View style={styles.container}>
      <Text>Practical 2</Text>
      <Text>Stateless and Statefull Components</Text>
      <Text style={styles.text}> 
            Yor are ready to start the journey.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
