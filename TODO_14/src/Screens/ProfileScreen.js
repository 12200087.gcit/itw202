import { View, Text, Image } from 'react-native'
import React from 'react'
import Header from '../components/Header'
import Background from '../components/Background'

export default function ProfileScreen() {
  return (
    <Background>
        <Header>You are in Profile Screen now.</Header>
        <Image style={{width:100, height:100, borderRadius:20}}
        source={require('../../assets/profile.jpg')}
        />
    </Background>
  )
}